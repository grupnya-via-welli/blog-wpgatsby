import React from "react"
import Layout from "../components/layout"
import { graphql } from "gatsby"

export default function BlogPost({ data }) {
    const post = data.allWpPost.nodes[0]

    //const featured = post.featuredImage.post.sourceUrl
    console.log(post)

    return ( <
        Layout >
        <
        div >
        <
        h2 > { post.title } < /h2>  <
        h3 > { post.date } < /h3> <
        style >
        @import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap'); <
        /style> <
        description >
        <
        p > < /p> <
        div dangerouslySetInnerHTML = {
            { __html: post.content }
        }
        />  < /
        description > < /
        div > <
        /Layout>
    )
}
export const query = graphql `
  query($slug: String!) {
    allWpPost(filter: { slug: { eq: $slug } }) {
      nodes {
        title
        date(formatString: "DD MMMM YYYY")
        content
      }
    }
  }
`