import React from "react"
import { Link, graphql } from "gatsby" //highlight-line
import Layout from "../components/layout"
import SEO from "../components/seo"

export default function Home({ data }) {


    return ( <
        Layout >
        <
        SEO title = "Home Via" / > {
            data.allWpPost.nodes.map(node => ( <
                div key = { node.slug } > { /* highlight-start */ } <
                style >
                @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap'); <
                /style><
                h2 > { node.title } < /h2>  <
                style >
                @import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap'); <
                /style> <
                h3 > { node.date } < /h3> <
                img style = {
                    { width: '18rem' }
                }
                src = { node.featuredImage.node.sourceUrl }
                alt = "Tampil" /
                >
                <
                image > { node.image } < /image><
                style >
                @import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap'); <
                /style> <
                h4 > <
                div dangerouslySetInnerHTML = {
                    { __html: node.excerpt }
                }
                />  < /
                h4 > <
                style >
                @import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap'); <
                /style> <
                Link to = { node.slug } > <
                button > Detail < /
                button > < /
                Link > < /div >
            ))
        } <
        /Layout>
    )
}

export const pageQuery = graphql `
  query {
    allWpPost(sort: {fields: date}) {
      nodes {
        featuredImage {
          node {
            sourceUrl
          }
        }
        title
        date(formatString: "DD MMMM YYYY")
        excerpt
        slug
      }
    }
  }
`