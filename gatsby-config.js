module.exports = {
    siteMetadata: {
        title: `Blog WordPress`,
        description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
        author: `@gatsbyjs`,
    },

    plugins: [

        `gatsby-plugin-react-helmet`,
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/images`,
            },
        },
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`,
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `gatsby-starter-default`,
                short_name: `starter`,
                start_url: `/`,
                background_image: 'src/images/meneliti.jpg',
                background_color: `#663399`,
                theme_color: `#663399`,
                display: `minimal-ui`,
                icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
            },
        },

        {
            resolve: `gatsby-source-wordpress-experimental`,
            options: {
                /*
                 * The full URL of the WordPress site's GraphQL API.
                 * Example : 'https://www.example-site.com/graphql'
                 */
                url: `http://wordpress-gatsbyjs.local/graphql/`,
            },
        },
    ],
}